<?php
define('DEBUG', true);

/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 9:03 PM
 */
require_once __DIR__ . "/control/meredithsInventoryDb.php";
$inventoryDb = new meredithsInventoryDb();
?>

<!DOCTYPE html>
<html>
<head>
    <title>My Usborne Inventory</title>
    <?php require_once __DIR__ . "/control/head.php";?>
</head>
<body>

<?php
//populating left sidebar
require_once __DIR__ . "/assets/php/leftSideBar.php";
?>
<!--this content is pushed out of the way when the side menu is pushed-->
<div class="backgroundImage">

    <?php
    //populating top nav
    require_once __DIR__ . "/assets/php/topNavBar.php";
    ?>
    <div id="cardContainer" class="ui container">
        <h1 class="ui header">Home</h1>
        <div class="ui five doubling cards">
            <?php require __DIR__ . "/assets/php/genreCards.php";?>
        </div>
    </div>
</div>
</body>

</html>
