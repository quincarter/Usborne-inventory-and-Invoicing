<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 8:59 PM
 */
session_start();
require __DIR__ . "/base.php";
?>

<!--<base id="base" href="localhost<?php /*echo $baseURL;*/?>">-->
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link rel="stylesheet" type="text/css" href="semantic/dist/semantic.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/semanticOverrides.css?ver=1.3">
<script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>

<script src="semantic/dist/semantic.min.js"></script>
<script src="assets/js/semanticControls.js"></script>

<script src="assets/js/fuse.js"></script>
<script src="assets/js/backstretch.js"></script>
<script src="ckeditor/ckeditor.js"></script>