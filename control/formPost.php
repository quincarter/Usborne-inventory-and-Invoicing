<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/18/2017
 * Time: 1:32 PM
 */
header("Content-Type: application/json; charset=utf-8");
require_once __DIR__ . "/meredithsInventoryDb.php";
$InventoryDb = new meredithsInventoryDb();
session_start();
//echo "posted data";
//echo $_POST['jsonarraydata'];
if(isset($_POST['jsonarraydata']))
{
    $postItems = $_POST['jsonarraydata'];
    $postItems = json_decode($postItems, true);
    $i=0;
    $arrayBuild = array();
    $authorFirstName = "";
    $authorLastName = "";
    $isbn = "";
    $genre = 0;

    foreach ($postItems as $postItem => $item)
    {
        $arrayBuild[$postItem] = array($item["name"] => $item["value"]);

        if($item["name"] == "firstName")
        {
            if(!empty($item["value"]))
            {
                $authorFirstName = $item["value"];
            }
            else
            {
                $authorFirstName = 'N/A';
            }
        }
        else if($item["name"] == "lastName")
        {
            if(!empty($item["value"]))
            {
                $authorLastName = $item["value"];
            }
            else
            {
                $authorLastName = 'N/A';
            }
        }
        else if($item["name"] == "genre")
        {
            $genre = $item["value"];
        }
        else if($item["name"] == "isbnNumber")
        {
            $isbn = $item["value"];
        }
    }

    //checking to see if author exists, then getting the ID. If the author does not exist, it will be inserted
    $getAuthorID = $InventoryDb->getAuthorID($authorFirstName, $authorLastName);

    //if it isn't numeric, it is a new genre.
    // The values are stored on the dropdown menu
    // and will be passed in if it is a current genre
    if(!is_numeric($genre))
    {
        //returns a new genreID to pass into the new insert query
        $genre = $InventoryDb->getGenreID($genre);
    }

    uploadImage($isbn);

    //checking to see if the inventory exists -- returns true or false
    $doesInventoryExist = $InventoryDb->checkInventoryExists($postItems, $getAuthorID, $genre);

    $result = $doesInventoryExist;



    echo json_encode($result);
    return json_encode($result);
}

function uploadImage($isbn)
{
    define("UPLOAD_DIR", "/var/www/html/draftdragon/inventory/bookImages/");
    if (!empty($_FILES["bookImage"])) {
        $bookImage = $_FILES["bookImage"];

        if ($bookImage["error"] !== UPLOAD_ERR_OK) {
            echo "<p>An error occurred.</p>";
            exit;
        }

        // ensure a safe filename
        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $bookImage["name"]);

        // don't overwrite an existing file
        $i = 0;
        $parts = pathinfo($name);
        while (file_exists(UPLOAD_DIR . $name)) {
            $i++;
            $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
        }

        // preserve file from temporary directory
        $success = move_uploaded_file($bookImage["tmp_name"],
            UPLOAD_DIR . $name);
        if (!$success) {
            echo "<p>Unable to save file.</p>";
            exit;
        }

        // set proper permissions on the new file
        chmod(UPLOAD_DIR . $name, 0644);
    }
}