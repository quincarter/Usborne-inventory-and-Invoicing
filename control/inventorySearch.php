<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/20/2017
 * Time: 9:52 PM
 */

require_once __DIR__ . "/meredithsInventoryDb.php";
$inventoryDB = new meredithsInventoryDb();

if(isset($_GET["key"]))
{

    /*
     * The array is actually built in the Class and is passed back
     * already formatted and configured for json
     */
    $query = $_GET["key"];
    $books = $inventoryDB->inventorySearch($query);

    $books = array("books" => $books);

    echo json_encode($books);
    return json_encode($books);
}
