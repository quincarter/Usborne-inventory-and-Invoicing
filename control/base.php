<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 9:01 PM
 */
$baseURL = "/inventory/";

function getSessionValue($key)
{
    if(array_key_exists($key, $_SESSION))
    {
        return $_SESSION[$key];
    }
    return "";
}

function getIsSelected($key, $value)
{
    if(array_key_exists($key, $_SESSION) && $_SESSION[$key] == $value)
    {
        return "selected";
    }
    return "";
}