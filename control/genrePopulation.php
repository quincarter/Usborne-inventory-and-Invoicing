<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/18/2017
 * Time: 12:13 AM
 */

$genres = $inventoryDb->getGenresForDropDown();

foreach ($genres as $genre)
{
    $name = $genre["genreName"];
    $id = $genre["id"];
    echo "<div class=\"item\" data-value=$id><i class=\"book icon\"></i>$name</div>";
}