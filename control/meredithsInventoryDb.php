<?php

/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 11:43 PM
 */
class meredithsInventoryDb
{
    //private $host="192.168.86.105";
    private $host="localhost";
    private $port=3306;
    private $socket="";
    private $user="meredithWriter";
    private $password="rocket";
    private $dbname="meredithsInventory";
    private $con;


    public function __construct()
    {
        $this->con = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket)
        or die ('Could not connect to the database server' . mysqli_connect_error());
    }

    //this is what populates the genres in the dropdown on the form to enter inventory
    public function getGenresForDropDown()
    {
        /*
         * This query is the master for the dropdown to populate genres
         */
        $mainArray = array();
        $query = "SELECT id, genreName FROM meredithsInventory.genres ORDER BY genreName ASC";

        $i=0;
        $results = $this->con->query($query);
        if ($results->num_rows > 0) {
            while($row = $results->fetch_assoc())
            {
                $populationArray = array(
                  "id" => $row["id"],
                    "genreName" => $row["genreName"]
                );

                $mainArray[$i] = $populationArray;
                $i++;
            }
        }

        return $mainArray;
    }


    //one public function and 2 private functions that relate to authors. Call the public getAuthorID from another file to get the author ID
    public function getAuthorID($firstName, $lastName)
    {
        $firstName = trim($firstName);
        $lastName = trim($lastName);

        $firstName = filter_var($firstName, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);
        $lastName = filter_var($lastName, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);



        $result = "";
        $existsResult = $this->checkAuthorExists($firstName, $lastName);
        if ($existsResult === true)
        {
            $result = $this->getIDValue($firstName, $lastName);
        }

        $this->con->close();
        return $result;
    }

    private function checkAuthorExists($firstName, $lastName)
    {
        $newFirstname = str_replace("'", "%", $firstName);
        $newFirstname = str_replace("\"", "%", $newFirstname);
        $newFirstname = str_replace("*", "%", $newFirstname);
        $newFirstname = str_replace("(", "%", $newFirstname);
        $newFirstname = str_replace(")", "%", $newFirstname);
        $newFirstname = str_replace(".", "%", $newFirstname);
        $newFirstname = str_replace(",", "%", $newFirstname);
        $newFirstname = str_replace(" ", "%", $newFirstname);

        $newLastName = str_replace("'", "%", $lastName);
        $newLastName = str_replace("\"", "%", $newLastName);
        $newLastName = str_replace("*", "%", $newLastName);
        $newLastName = str_replace("(", "%", $newLastName);
        $newLastName = str_replace(")", "%", $newLastName);
        $newLastName = str_replace(".", "%", $newLastName);
        $newLastName = str_replace(",", "%", $newLastName);
        $newLastName = str_replace(" ", "%", $newLastName);
        
        $sql = "SELECT id 
                  FROM meredithsInventory.authors
                  WHERE REPLACE(REPLACE(REPLACE(firstName, '.', '%'), CHAR(32), '%'), ',', '%') 
                  LIKE '%$newFirstname%' 
                  AND 
                  REPLACE(REPLACE(REPLACE(lastName, '.', '%'), CHAR(32), '%'), ',', '%') 
                  LIKE '%$newLastName%';";
        $result = $this->con->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            $result = true;//author exists because the result returns more than 0 rows
        } else {
            $sqlInsert = "INSERT INTO authors (firstName, lastName)
                      VALUES ('$firstName', '$lastName');";
            if ($this->con->query($sqlInsert) === TRUE) {
                $result = true;//returning true because the author exists now
            } else {
                echo "Error: " . $sqlInsert . "<br>" . $this->con->error;
            }
        }

        //$this->con->close();

        return $result;
    }

    private function getIDValue($firstName, $lastName)
    {
        $firstName = str_replace("'", "%", $firstName);
        $firstName = str_replace("\"", "%", $firstName);
        $firstName = str_replace("*", "%", $firstName);
        $firstName = str_replace("(", "%", $firstName);
        $firstName = str_replace(")", "%", $firstName);
        $firstName = str_replace(".", "%", $firstName);
        $firstName = str_replace(",", "%", $firstName);
        $firstName = str_replace(" ", "%", $firstName);

        $lastName = str_replace("'", "%", $lastName);
        $lastName = str_replace("\"", "%", $lastName);
        $lastName = str_replace("*", "%", $lastName);
        $lastName = str_replace("(", "%", $lastName);
        $lastName = str_replace(")", "%", $lastName);
        $lastName = str_replace(".", "%", $lastName);
        $lastName = str_replace(",", "%", $lastName);
        $lastName = str_replace(" ", "%", $lastName);

        $resultSet = "";
        $sql = "SELECT id 
                  FROM meredithsInventory.authors
                  WHERE REPLACE(REPLACE(REPLACE(firstName, '.', '%'), CHAR(32), '%'), ',', '%') 
                  LIKE '%$firstName%' 
                  AND 
                  REPLACE(REPLACE(REPLACE(lastName, '.', '%'), CHAR(32), '%'), ',', '%') 
                  LIKE '%$lastName%';";
        $result = $this->con->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                //echo "id: " . $row["id"];
                $resultSet = $row["id"];
            }
        } else {
            echo "0 results";
        }

        //$this->con->close();

        return $resultSet;
    }


//one public function and 2 private functions that relate to genres. Call the public getGenreID from another file to get the genre
    public function getGenreID($genre)
    {
        $genreID = "";
        $genreInsertResult = $this->insertGenreToDB($genre);

        if($genreInsertResult || $genreInsertResult == "Already Exists")
        {
            $genreID = $this->getGenreIDValue($genre);
        }

        //$this->con->close();

        return $genreID;
    }

    private function insertGenreToDB($genre)
    {
        $dbConnect = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket);
        $resultSet = "";
        $sql = "SELECT id 
                  FROM meredithsInventory.genres
                  WHERE                  
                  genres.genreName = '$genre';";
        $result = $dbConnect->query($sql);

        if ($result->num_rows > 0) {
            $resultSet = "Already Exists";
        } else {
            $sql = "INSERT INTO genres (genreName)
                VALUES ('$genre')";

            if ($dbConnect->query($sql) === TRUE) {
                $resultSet = true;
            } else {
                echo "Error: " . $sql . "<br>" . $dbConnect->error;
            }
        }


        return $resultSet;
    }

    private function getGenreIDValue($genre)
    {
        $dbConnect = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket);
        $resultSet = "";
        $sql = "SELECT id 
                  FROM meredithsInventory.genres
                  WHERE genres.genreName = '$genre';";
        $result = $dbConnect->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                //echo "id: " . $row["id"];
                $resultSet = $row["id"];
            }
        } else {
            echo "0 results";
        }

        return $resultSet;
    }


//checking to see if the inventory exists in the system before adding it to eliminate duplicates.
//This also adds the inventory to the system if it doesn't exist
    public function checkInventoryExists($postItems, $authorID, $genreID)
    {
        $dbConnect = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket);
        $bookName = "";
        $isbn = "";
        $quantity = "";
        $description = "";
        $price = 0.00;

        foreach ($postItems as $postItem => $item)
        {
            //$arrayBuild[$postItem] = array($item["name"] => $item["value"]);

            if($item["name"] == "bookName")
            {
                $bookName = $item["value"];
            }
            else if($item["name"] == "isbnNumber")
            {
                $isbn = $item["value"];
            }
            else if($item["name"] == "quantity")
            {
                $quantity = $item["value"];
            }
            else if($item["name"] == "description")
            {
                $description = $item["value"];
            }

            else if($item["name"] == "bookPrice")
            {
                $price = $item["value"];
            }
        }

        //$description = filter_var($description, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);
        //$bookName = filter_var($bookName, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);

        $description = str_replace("'", '', $description);
        $bookName = str_replace("'", "", $bookName);

        $resultSet = "";
        $sql = "SELECT id 
                  FROM meredithsInventory.books
                  WHERE                  
                  books.isbn = '$isbn';";
        $result = $dbConnect->query($sql);

        if ($result->num_rows > 0) {
            $resultSet = "Already Exists";
        } else {
            //images are not inserted here
            $sql = "INSERT INTO books (authorID, genreID, isbn, bookName, description, quantity, lastModified, price)
                VALUES ($authorID, $genreID, $isbn, '$bookName', '$description', $quantity, current_timestamp(), $price)";

            if ($dbConnect->query($sql) === TRUE) {
                $resultSet = "Inventory Added";
            } else {
                echo "Error: " . $sql . "<br>" . $dbConnect->error;
            }
        }

        $dbConnect->close();

        return $resultSet;
    }


//This populates the books on the myInventory.php page.
//Books population occurs in assets/php/bookCards.php in a foreach loop
//Books that have a 0 quantity will not show here
    public function getBookInventory()
    {
        $resultSet = "";
        $mainArray = array();
        $dbConnect = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket);
        $sql = "SELECT books.id as BookID,
                      books.isbn as ISBNNumber,
                      books.image as BookImage,
                      books.bookName as BookName,
                      books.description as BookDescription,
                      books.quantity as Quantity,
                      CONCAT('$', format(books.price, 2)) as price,
                      
                      authors.firstName as AuthorFirstName,
                      authors.lastName as AuthorLastName,
                      CONCAT(authors.firstName, ' ', authors.lastName) as AuthorFullName,
                      
                      genres.genreName as Genre,
                      books.genreID as GenreID,
                      books.lastModified as lastModified,
                      CASE 				
                            WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) < 30
                            THEN 'Less than a Minute ago'
                            
                            WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) < 60
                            THEN 'A little over 30 seconds ago'
                            
                            WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) >= 60 AND time_to_sec(timediff(current_timestamp(), books.lastModified)) < 120
                            THEN 'About 1 minute ago'
                            
                            WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60) > 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60) < 60
                            THEN CONCAT('About ', FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60), ' minutes ago')
                             
                            WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) >= 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) < 2
                            THEN CONCAT('About an hour ago')
                            
                            WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) > 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) < 24
                            THEN CONCAT('About ', FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600), ' hours ago')
                            
                            WHEN DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) >= 1 AND DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) < 2
                            THEN 'About 1 day ago'
                            
                            WHEN DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) > 1 AND DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) < 30
                            THEN CONCAT('About ', DATE(current_timestamp()) - DATE(books.lastModified), ' days ago')
                            
                            WHEN period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) >= 1 
                                AND period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified)))	< 2
                            THEN 'About a month ago'
                            
                            WHEN period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) > 1
                                AND period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) < 12
                            THEN CONCAT('About ', period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))), ' months ago')
                            
                            ELSE
                                'A while ago'
                            END as lastModifiedText	
                            						
                  FROM meredithsInventory.books
                  JOIN genres ON genres.id = books.genreID
                  JOIN authors ON authors.id = books.authorID
                  
                  WHERE                  
                  books.quantity > 0
                  ORDER BY books.bookName ASC;";
        $result = $dbConnect->query($sql);

        if ($result->num_rows > 0) {
            $i=0;
            while($row = $result->fetch_assoc()) {
                $populationArray = array(
                    "BookID" => $row["BookID"],
                    "ISBNNumber" => $row["ISBNNumber"],
                    "BookImage" => $row["BookImage"],
                    "BookName" => $row["BookName"],
                    "BookDescription" => $row["BookDescription"],
                    "Quantity" => $row["Quantity"],
                    "price" => $row["price"],
                    "AuthorFullName" => $row["AuthorFullName"],
                    "Genre" => $row["Genre"],
                    "GenreID" => $row["GenreID"],
                    "LastModified" => $row["lastModifiedText"]
                );

                $mainArray[$i] = $populationArray;
                $i++;
            }
            $resultSet = $mainArray;
        } else {
            $resultSet = "No Books in Stock";
            $_SESSION["inventoryResultSet"] = $resultSet;
        }
        $_SESSION["inventoryResultSet"] = $resultSet;
        return $resultSet;
    }



//This is used to search all inventory items in the database.
//The query will return results based on partial data in multiple columns as well.
//Search happens on every page and is populated on the pages with assets/php/topNavBar.php
//JS files are in that php file as well.
    public function inventorySearch($searchQuery)
    {
        $resultSet = "";
        $mainArray = array();
        $dbConnect = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket);
        $sql = "SELECT books.id as BookID,
                      books.isbn as ISBNNumber,
                      books.image as BookImage,
                      books.bookName as BookName,
                      books.description as BookDescription,
                      books.quantity as Quantity,
                      CONCAT('$', format(books.price, 2)) as price,
                      
                      authors.firstName as AuthorFirstName,
                      authors.lastName as AuthorLastName,
                      CONCAT(authors.firstName, ' ', authors.lastName) as AuthorFullName,
                      
                      genres.genreName as Genre,
                      books.genreID as GenreID,
                      books.lastModified as lastModified,
                                            
                      CASE 
						WHEN MONTH(current_timestamp()) = MONTH(books.lastModified)
								AND DAY(current_timestamp()) = day(books.lastModified)
                                AND YEAR(current_timestamp()) = YEAR(books.lastModified)
							THEN 
								CASE 
									WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) < 30
                                    THEN 'Less than a Minute ago'
									WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) < 60
                                    THEN 'A little over 30 seconds ago'
                                    WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) > 60 AND time_to_sec(timediff(current_timestamp(), books.lastModified)) < 120
                                    THEN 'About 1 minute ago'
                                    WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60) > 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60) < 60
                                    THEN CONCAT('About ', FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60), ' minutes ago')
                                    WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) >= 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) < 2
                                    THEN CONCAT('About an hour ago')
                                    WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) > 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) < 24
                                    THEN CONCAT('About ', FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600), ' hours ago')
                                    WHEN DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) >= 1 AND DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) < 2
                                    THEN 'About 1 day ago'
                                    WHEN DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) > 1 AND DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) < 30
                                    THEN CONCAT('About ', DATE(current_timestamp()) - DATE(books.lastModified), ' days ago')
                                    WHEN period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) >= 1 
										AND period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified)))	< 2
									THEN 'About a month ago'
                                    WHEN period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) > 1
										AND period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) < 12
									THEN CONCAT('About ', period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))), ' months ago')
                                    ELSE
										'A while ago'
                                    END
							END as lastModifiedText							
                  FROM meredithsInventory.books
                  JOIN genres ON genres.id = books.genreID
                  #AND books.quantity > 0
                  JOIN authors ON authors.id = books.authorID
                  
                  WHERE
                    (
						books.isbn LIKE CONCAT('%', REPLACE(REPLACE(REPLACE('$searchQuery', CHAR(32), '%'), '-', '%'), '*', '%'), '%')
                        OR
						books.bookName LIKE CONCAT('%', REPLACE(REPLACE(REPLACE('$searchQuery', CHAR(32), '%'), '-', '%'), '*', '%'), '%')
						OR
                        authors.firstName LIKE CONCAT('%', REPLACE(REPLACE(REPLACE('$searchQuery', CHAR(32), '%'), '-', '%'), '*', '%'), '%')
                        OR
                        authors.lastName LIKE CONCAT('%', REPLACE(REPLACE(REPLACE('$searchQuery', CHAR(32), '%'), '-', '%'), '*', '%'), '%')
                        OR
                        concat(authors.firstName, ' ', authors.lastName) LIKE CONCAT('%', REPLACE(REPLACE(REPLACE('$searchQuery', CHAR(32), '%'), '-', '%'), '*', '%'), '%')
                        OR
                        genres.genreName LIKE CONCAT('%', REPLACE(REPLACE(REPLACE('$searchQuery', CHAR(32), '%'), '-', '%'), '*', '%'), '%')
                        OR
						books.description LIKE CONCAT('%', REPLACE(REPLACE(REPLACE('$searchQuery', CHAR(32), '%'), '-', '%'), '*', '%'), '%')				  						
                    )
				";
        $result = $dbConnect->query($sql);

        if ($result->num_rows > 0) {
            $i=0;
            while($row = $result->fetch_assoc()) {
                $populationArray = array(
                    "BookID" => $row["BookID"],
                    "ISBNNumber" => $row["ISBNNumber"],
                    "BookImage" => $row["BookImage"],
                    "BookName" => $row["BookName"],
                    "BookDescription" => $row["BookDescription"],
                    "Quantity" => $row["Quantity"],
                    "price" => $row["price"],
                    "AuthorFullName" => $row["AuthorFullName"],
                    "Genre" => $row["Genre"],
                    "GenreID" => $row["GenreID"],
                    "LastModified" => $row["lastModifiedText"]
                );

                $mainArray[$i] = $populationArray;
                $i++;
            }
            $resultSet = $mainArray;
            $_SESSION["inventoryResultSet"] = $resultSet;
        } else {
            $resultSet = "No Books to view in the database";
        }
        return $resultSet;
    }

//Getting books searched by genre if the genre key is given in the URL
    public function inventoryGenreSearch($genreQuery)
    {
        $resultSet = "";
        $mainArray = array();
        $dbConnect = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket);
        $sql = "SELECT books.id as BookID,
                      books.isbn as ISBNNumber,
                      books.image as BookImage,
                      books.bookName as BookName,
                      books.description as BookDescription,
                      books.quantity as Quantity,
                      CONCAT('$', format(books.price, 2)) as price,
                      
                      authors.firstName as AuthorFirstName,
                      authors.lastName as AuthorLastName,
                      CONCAT(authors.firstName, ' ', authors.lastName) as AuthorFullName,

                      genres.genreName as Genre,
                      books.genreID as GenreID,
                      books.lastModified as lastModified,

                      CASE
						WHEN MONTH(current_timestamp()) = MONTH(books.lastModified)
								AND DAY(current_timestamp()) = day(books.lastModified)
                                AND YEAR(current_timestamp()) = YEAR(books.lastModified)
							THEN
								CASE
									WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) < 30
                                    THEN 'Less than a Minute ago'
									WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) < 60
                                    THEN 'A little over 30 seconds ago'
                                    WHEN time_to_sec(timediff(current_timestamp(), books.lastModified)) > 60 AND time_to_sec(timediff(current_timestamp(), books.lastModified)) < 120
                                    THEN 'About 1 minute ago'
                                    WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60) > 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60) < 60
                                    THEN CONCAT('About ', FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/60), ' minutes ago')
                                    WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) >= 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) < 2
                                    THEN CONCAT('About an hour ago')
                                    WHEN FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) > 1 AND FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600) < 24
                                    THEN CONCAT('About ', FLOOR(time_to_sec(timediff(current_timestamp(), books.lastModified))/3600), ' hours ago')
                                    WHEN DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) >= 1 AND DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) < 2
                                    THEN 'About 1 day ago'
                                    WHEN DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) > 1 AND DATEDIFF(DATE(current_timestamp()), DATE(books.lastModified)) < 30
                                    THEN CONCAT('About ', DATE(current_timestamp()) - DATE(books.lastModified), ' days ago')
                                    WHEN period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) >= 1
										AND period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified)))	< 2
									THEN 'About a month ago'
                                    WHEN period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) > 1
										AND period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))) < 12
									THEN CONCAT('About ', period_diff(CONCAT(YEAR(current_timestamp()), MONTH(current_timestamp())), CONCAT(YEAR(books.lastModified), month(books.lastModified))), ' months ago')
                                    ELSE
										'A while ago'
                                    END
							END as lastModifiedText
                  FROM meredithsInventory.books
                  JOIN genres ON genres.id = books.genreID                  
                  JOIN authors ON authors.id = books.authorID
                                                
                  WHERE genres.genreName = '$genreQuery'                                                
				";
        $result = $dbConnect->query($sql);

        if ($result->num_rows > 0) {
            $i=0;
            while($row = $result->fetch_assoc()) {
                $populationArray = array(
                    "BookID" => $row["BookID"],
                    "ISBNNumber" => $row["ISBNNumber"],
                    "BookImage" => $row["BookImage"],
                    "BookName" => $row["BookName"],
                    "BookDescription" => $row["BookDescription"],
                    "Quantity" => $row["Quantity"],
                    "price" => $row["price"],
                    "AuthorFullName" => $row["AuthorFullName"],
                    "Genre" => $row["Genre"],
                    "GenreID" => $row["GenreID"],
                    "LastModified" => $row["lastModifiedText"]
                );

                $mainArray[$i] = $populationArray;
                $i++;
            }
            $resultSet = $mainArray;
            $_SESSION["inventoryResultSet"] = $resultSet;
        } else {
            $resultSet = "No Books to view in the database";
        }
        return $resultSet;
    }

//getting genres for the home page
    public function getGenres()
    {
        $resultSet = "";
        $mainArray = array();
        $dbConnect = new mysqli($this->host, $this->user, $this->password, $this->dbname, $this->port, $this->socket);
        $sql = "SELECT
                  genres.genreName as genreName,
                  genres.shortName as pictureName,
                  COUNT(books.bookName) as bookTotals
                FROM books
                JOIN genres ON genres.id = books.genreID
                
                GROUP BY genres.genreName, genres.shortName;";
        $result = $dbConnect->query($sql);

        if ($result->num_rows > 0) {
            $i=0;
            while($row = $result->fetch_assoc()) {
                $populationArray = array(
                    "Genre" => $row["genreName"],
                    "Picture" => $row["pictureName"],
                    "Total" => $row["bookTotals"]
                );

                $mainArray[$i] = $populationArray;
                $i++;
            }
            $resultSet = $mainArray;
        } else {
            $resultSet = "No Books in Stock";
            $_SESSION["inventoryResultSet"] = $resultSet;
        }
        $_SESSION["inventoryResultSet"] = $resultSet;
        return $resultSet;
    }

    public function getInvoiceCart()
    {
        return "";
    }


}
