<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 9:03 PM
 */
require_once __DIR__ . "/control/meredithsInventoryDb.php";
$inventoryDb = new meredithsInventoryDb();
?>

<!DOCTYPE html>
<html>
<head>
    <title>My Usborne Inventory</title>
    <?php require_once __DIR__ . "/control/head.php";?>
</head>
<body>
<?php
//populating left sidebar
require_once __DIR__ . "/assets/php/leftSideBar.php";
?>
    <div class="backgroundImage">
        <?php
        //populating top nav
        require_once __DIR__ . "/assets/php/topNavBar.php";
        ?>
        <div id="cardContainer" class="ui container">
            <h1 class="ui header">
                <?php
                if(isset($_GET["searchTerm"]))
                {
                    echo "Search Results";
                }
                else
                {

                    echo "Inventory Items";

                }
                ?>
            </h1>
            <div class="ui five doubling cards">
                <?php require __DIR__ . "/assets/php/bookCards.php";?>
            </div>

        </div>
        <?php
        if($_SESSION["inventoryResultSet"] == "No Books in Stock" && !isset($_GET["searchTerm"]))
        {
            ?>
            <div id="nothingToSeeHere" class="ui raised very padded text container segment">
                <h1 class="ui header">No Inventory to see here!</h1>
                <div class="ui feed">
                    <div class="event">
                        <div class="content">
                            <div class="summary">
                                <a href="form">Click here to add inventory</a>
                                <BR><BR>Inventory will be added to this page automatically as items are entered in the <a href="form">form</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>


    </div>
<!--</div>-->

</body>

</html>
