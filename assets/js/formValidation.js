/**
 * Created by Quin on 4/17/2017.
 */
/**
 * Created by carterlq on 3/30/2017.
 */
$.fn.form.settings.templates.error =
    function(errors)
    {
        var html = '<div class="ui error header">Error! Form Not Completed!</div><div class="ui divider"></div><div class="ui list">';
        html += '<ul class="list">';
        $.each(errors, function(index, value) {
            html += '<li>' + value + '</li>';
        });
        html += '</ul></div>';
        return $(html);
    };

$('.ui.form')
    .form({
        fields: {
            bookName: {
                identifier: 'bookName',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter a Book Name'
                    }
                ]
            },
            isbnNumber: {
                identifier: 'isbnNumber',
                rules: [
                    {
                        type   : 'exactLength[6]',
                        prompt : 'Please enter a valid ISBN# (exactly 6 characters)'
                    }
                ]
            },
            genre: {
                identifier: 'genre',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter a valid Genre'
                    }
                ]
            },
            quantity: {
                identifier: 'quantity',
                rules: [
                    {
                        type   : 'integer',
                        prompt : 'Please enter a quantity greater than 0'
                    }
                ]
            },
            /*firstName: {
                identifier: 'firstName',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter the Author\'s First Name'
                    }
                ]
            },
            lastName: {
                identifier: 'lastName',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter the Author\'s Last Name'
                    }
                ]
            },*/
            Reason: {
                identifier: 'Reason',
                rules: [
                    {
                        type   : 'checked',
                        prompt : 'Please select a valid concern reason'
                    }
                ]
            },

            additionalComments: {
                identifier: 'additionalComments',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please write a brief description about the book'
                    }
                ]
            }
        }
    })
;

