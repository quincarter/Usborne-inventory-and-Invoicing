/**
 * Created by Quin on 4/17/2017.
 */
$(document).ready(function () {
    var sidebarToggle = $('#sidebarToggle');

    sidebarToggle.on('click', function () {
        $(document).scrollTop(0);
        $('.ui.labeled.icon.sidebar').sidebar('toggle');
    });

    $("#collapsedMenu").dropdown();
    $("#inventoryTopNavMenu").dropdown();

    $("#searchableGenreSelection").dropdown({allowAdditions: true});

    $(".blurring.dimmable.image.blurrableBookImage").dimmer({
       on: 'hover'
    });
});