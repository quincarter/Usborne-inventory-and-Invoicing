$(document).ready(function () {

    $('.ui.search')
        .search({
            //source: content,
            type: 'category',
            fullTextSearch: true,
            cache: false,
            //searchFields: ['name', 'org'],
            minCharacters : 2,
            maxResults : 8,
            apiSettings   : {
                url: 'control/inventorySearch.php?key={query}',
                onResponse: function(booksDatabaseResponse) {
                    // returns results html for category results

                    console.log(booksDatabaseResponse);
                    var key = $("#searchInputBox").val();
                    booksDatabaseResponse = sortRelevantSearchResults(booksDatabaseResponse, key);

                    var
                        response = {
                            results : {}
                        };
                    // translate GitHub API response to work with search
                    $.each(booksDatabaseResponse.books, function(index, book) {
                        var
                            Genre   = book.Genre || 'Unknown',
                            maxResults = 5
                        ;
                        if(index >= maxResults) {
                            return false;
                        }
                        // create new categoryName category
                        if(response.results[Genre] === undefined) {
                            response.results[Genre] = {
                                name    : Genre,
                                results : []
                            };
                        }

                        // add result to category
                        response.results[Genre].results.push({
                            title       : book.BookName,
                            description : 'ISBN: ' + book.ISBNNumber + '  |  <strong>Quantity: ' + book.Quantity + '</strong><BR><BR>' + book.BookDescription,
                            image : 'assets/img/BookCovers/' + book.ISBNNumber + '.jpeg',
                            url     : 'myinventory.php?searchTerm=' + book.ISBNNumber
                        });
                    });


                    return response;
                }
            }
        });

    function sortRelevantSearchResults(data, query) {
        //mapping the data property to the new array variable.
        //javascript was stripping the identifier out of the JSON object and parsing it as a string
        //this maps the data to a new array.
        var arr = $.map(data, function(el) { return el });

        var options = {
            caseSensitive: false,
            includeScore: true,
            shouldSort: true,
            tokenize: true,
            //verbose: true,
            threshold: 0.6,
            //location: 0,
            distance: 100,
            maxPatternLength: 32,
            keys: ["BookName", "ISBNNumber", "BookDescription", "AuthorFullName", "Genre"]
        };

        var fuse = new Fuse(arr, options); //creates new fuse class and passes the new array and options into the class
        var result = fuse.search(query);// getting new result from the fuse class and it will sort the results by relevance.

        result = {books: result};//had
        return result;
    }
});