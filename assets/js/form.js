/**
 * Created by Quin on 4/18/2017.
 */
$(document).ready(function () {
    $("#bookNameField").focus();
   $("#inventoryForm").submit(function (event) {
       event.preventDefault();
       var jsonarray = JSON.stringify($("#inventoryForm").serializeArray());
       console.log(jsonarray);
       //$form = $(this);
       $.ajax({
           type: 'POST', // set sending HTTP Request type
           url: 'control/formPost.php', // php script to return json encoded string
           data: {'jsonarraydata': jsonarray},  // serialized data to send on server

           success: function (returnedData) {
               console.log(returnedData);

               if (returnedData === "Inventory Added")
               {
                   //$("#imageUploadForm").submit();
                   $("#inventoryForm")[0].reset();
                   location.reload();
               }
               else if (returnedData === "Already Exists")
               {
                   alert("ISBN Number Already Exists in the Database.");
                   $("#inventoryForm")[0].reset();
                   //location.reload();
               }
           }
       });
       return false;
   });
});