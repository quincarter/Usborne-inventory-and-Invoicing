<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 10:42 PM
 */

echo "<div class=\"ui left vertical inverted sidebar labeled icon menu\">
    <a href=\"form\" class=\"item\">
        <i class=\"pencil icon\"></i>
        Log<BR>Inventory
    </a>
    <a href=\"myinventory\" class=\"item\">
        <i class=\"suitcase icon\"></i>
        View<BR>Inventory
    </a>
    <a href=\"invoices\" class=\"item\">
        <i class=\"credit card alternative icon\"></i>
        Invoices
    </a>
</div>";