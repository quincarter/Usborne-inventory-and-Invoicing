<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 10:39 PM
 */

echo "

    <div id=\"mainTopBar\" class=\"ui top fixed menu\">
            <button id=\"sidebarToggle\" class=\"ui button icon item\">
                <i id=\"hamburgerButton\" class=\"content icon\"></i>
            </button>
    
            <form method='get' action='myinventory' name='topNavSearchForm' class='form' id='topNavSearchForm'>
                <div id=\"searchBox\" class=\"ui fluid category search\">
                    <div class=\"ui fluid icon input\">
                        <input id='searchInputBox' class=\"prompt\" type=\"text\" name=\"searchTerm\" placeholder=\"Search Inventory...\">
                        <i class=\"search icon\"></i>
                    </div>
                    <div class=\"results\"></div>
                </div>
//            </form>
            
            <script src=\"assets/js/semanticSearch.js\"></script>
    
            <div class=\"right menu\">
                <a href=\"index\" id=\"topBarHomeButton\" class=\"ui wide basic button\" title='Home'><i class=\"home icon\"></i></a>
                <div id=\"inventoryTopNavMenu\" class=\"ui icon top right pointing dropdown basic button\" title='Inventory Dropdown'>
                    <i class=\"suitcase icon\"></i>
                    <div class=\"menu\">
                        <a href='form' class=\"item\"><i class=\"plus icon\"></i>Add to inventory</a>
                        <a href='myinventory' class=\"item\"><i class=\"suitcase icon\"></i>View My Inventory</a>  
                    </div>
                </div>                
                <a href=\"invoices\" id=\"topBarInvoiceButton\" class=\"ui wide basic button\" title='Current Invoices'><i class=\"cart icon\"></i></a>                
            </div>
            
<div id=\"collapsedMenu\" class=\"ui icon top right pointing dropdown basic button\">
  <i class=\"vertical ellipsis icon\"></i>
  <div class=\"menu\">
  <div class=\"header\">General</div>
    <a href='index' class=\"item\"><i class=\"home icon\"></i>Home</a>   
    <a href='invoices' class=\"item\"><i class=\"cart icon\"></i>Current Invoices</a>  
    <div class=\"ui divider\"></div> 
         <div class=\"header\">Inventory</div>
    <a href='form' class=\"item\"><i class=\"plus icon\"></i>Add to inventory</a>
    <a href='myinventory' class=\"item\"><i class=\"suitcase icon\"></i>View My Inventory</a>  
  </div>
</div>
    </div>

";