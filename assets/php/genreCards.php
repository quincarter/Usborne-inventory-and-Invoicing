<?php
    $genres = $inventoryDb->getGenres();

    foreach ($genres as $genre)
    {
        $genreName = $genre["Genre"];
        $image = $genre["Picture"];
        $total = $genre["Total"];
        $searchLink = 'myinventory?genre=' . $genreName;
        $filename = 'assets/img/Genres/'. $image . '.png'; //checking to see if the image file exists ISBN#.jpeg (6 digit usborne inventory code)

        if (file_exists($filename)) {
            $imagePath = "assets/img/Genres/". $image . ".png";
        } else {
            $imagePath = "assets/img/imagePlaceholder.png"; //echoing the prettier image placeholder if the image is missing
        }

        echo "<form class='ui link card form'>
              <a class=\"image blurrableBookImage\" href=\"$searchLink\">                
                        <img src=\"$imagePath\">
              </a>
              
              <div class=\"content\">              
                <a class=\"header\" href=\"$searchLink\">$genreName</a>
                <div class=\"meta\">                 
                  <strong>$total</strong> Books                  
                </div>
              </div>             
        </form>";
    }


