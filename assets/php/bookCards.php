<?php

$books = array();

if(!isset($_GET["searchTerm"]) && !isset($_GET['genre']))
{
    $books = $inventoryDb->getBookInventory();
}

else if (isset($_GET["searchTerm"]) && !isset($_GET['genre']))
{
    $getSpecificItem = $_GET["searchTerm"];
    $books = $inventoryDb->inventorySearch($getSpecificItem);
}
else if (!isset($_GET["searchTerm"]) && isset($_GET['genre']))
{
    $getSpecificItem = $_GET["genre"];
    $books = $inventoryDb->inventoryGenreSearch($getSpecificItem);
}
    foreach ($books as $book)
    {
        $bookName = $book["BookName"];
        $isbn = $book["ISBNNumber"];
        $quantity = $book["Quantity"];
        $modified = $book["LastModified"];
        $bookID = $book["BookID"];
        $price = $book["price"];
        $description = $book["BookDescription"];
        //$storeLink = 'https://d5531.myubam.com/search?q=' . $isbn;
        $storeLink = 'myinventory?searchTerm=' . $isbn;
        $filename = 'assets/img/BookCovers/'. $isbn . '.jpeg'; //checking to see if the image file exists ISBN#.jpeg (6 digit usborne inventory code)

        if (file_exists($filename)) {
            $imagePath = "assets/img/BookCovers/". $isbn . ".jpeg";
        } else {
            $imagePath = "assets/img/imagePlaceholder.png"; //echoing the prettier image placeholder if the image is missing
        }

        echo "<form method='post' class='ui link card form' id='EditBookInfo$bookID'>
              <a class=\"blurring dimmable image blurrableBookImage\" href=\"$storeLink\">
                <div class=\"ui dimmer\">
                        <div class=\"content\">
                            <div class=\"center two buttons\">
                                <button id=\"EditBookInfo$bookID\" class=\"ui inverted button\" value=\"$bookID\"><i class='pencil icon'></i>Edit Book Info</button>
                                <button id=\"CreateInvoice$bookID\" class=\"ui inverted button\" value=\"$bookID\"><i class='add to cart icon'></i>Add to Invoice</button>
                            </div>
                        </div>
                    </div>
                        <img src=\"$imagePath\">
              </a>
              
              <div class=\"content\">
              <!--<form method='post' class='form' id='EditBookInfo$bookID'>-->
                <a class=\"header\" href=\"$storeLink\">$bookName</a>
                <div class=\"meta\">
                  <div class=\"right floated meta\"><strong class='greenMoney'>$price</strong></div>
                  <strong>$quantity</strong> in stock
                  <BR>ISBN: $isbn
              <!--</form>-->
                </div>
              </div>             
        </form>";
    }


