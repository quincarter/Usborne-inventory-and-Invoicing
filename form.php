<?php
/**
 * Created by PhpStorm.
 * User: Quin
 * Date: 4/17/2017
 * Time: 9:03 PM
 */

require_once __DIR__ . "/control/meredithsInventoryDb.php";
//creating an instance of this class to get the database connection
$inventoryDb = new meredithsInventoryDb();

?>

<!DOCTYPE html>
<html>
<head>
    <title>My Usborne Inventory</title>
    <?php require_once __DIR__ . "/control/head.php";?>
    <script src="assets/js/form.js"></script>
</head>
<body>
<?php
//populating left sidebar
require_once __DIR__ . "/assets/php/leftSideBar.php";
?>
<!--this content is pushed out of the way when the side menu is pushed-->
<div class="backgroundImage">
    <?php
    //populating top nav
    require_once __DIR__ . "/assets/php/topNavBar.php";
    ?>
    <div class="ui raised very padded text container segment">
        <div id="mainTitleHeader" class="ui header centered">
            <h1 class="ui header">Inventory Entry</h1>
        </div>

        <div class="ui divider"></div>

        <form name="inventoryForm" id="inventoryForm" class="ui form revalidate" method="POST" enctype="multipart/form-data">
            <div class="ui equal width form">
                <div class="ui header left">
                    Book Information
                </div>
                <div class="fields">
                    <div class="required field">
                        <label>Book Name</label>
                        <input id="bookNameField" name="bookName" data-validate="bookName" type="text" placeholder="Book Name">
                    </div>
                    <div class="required field">
                        <label>ISBN#</label>
                        <input name="isbnNumber" data-validate="isbnNumber" type="text" placeholder="ISBN Number">
                    </div>
                </div>
                <div class="required field">
                    <label>Genre <BR>(new items can be added here and they will be saved)</label>
                    <div id="searchableGenreSelection" class="ui fluid search selection dropdown">
                        <input type="hidden" name="genre" data-validate="genre">
                        <i class="dropdown icon"></i>
                        <div class="default text">Genre</div>
                        <div class="menu">
                            <!--Genres Populate Here from the database more will add when she adds more-->
                            <?php require_once __DIR__ . "/control/genrePopulation.php";?>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <!--<form id="imageUploadForm" action="control/fileUpload.php" name="imageUpload" class="ui form" method="POST" enctype="multipart/form-data">
                        <div class="required field">
                            <label>Image Of the Book</label>
                            <input class="button" name="bookImage" type="file" id="fileinput" />
                        </div>
                    </form>-->
                    <i class="usd icon inline"></i>
                    <div class="required field">
                        <label>Price</label>
                        <input name="bookPrice" data-validate="bookPrice" type="text" placeholder="Price">
                    </div>
                    <div class="required field">
                        <label>Quantity In Stock (Greater than 0)</label>
                        <input name="quantity" data-validate="quantity" type="number" placeholder="Quantity">
                    </div>
                </div>

                <div class="ui header left">
                    Author Information
                </div>
                <div class="ui small text">Not Required but can be used to to filter searches</div><BR>

                <div class="fields">
                    <div class="field">
                        <label>First Name</label>
                        <input name="firstName" data-validate="firstName" type="text" placeholder="Author First Name">
                    </div>
                    <div class="field">
                        <label>Last Name</label>
                        <input name="lastName" data-validate="lastName" type="text" placeholder="Author Last Name">
                    </div>
                </div>
                <!--<div class="ui divider"></div>
                <div class="grouped fields">
                    <div class="ui header">
                        Bullets go below here<BR>
                    </div>
                </div>-->
                <div class="ui divider"></div>
                <div class="ui header">
                    Description<BR>
                </div>
                <div class="fields">
                    <div class="required field">
                        <label>A Brief summary about the book or something that describes the book <br>(limit 1000 characters)</label>
                        <textarea maxlength="1000" id="descriptionTextBox" name="description" data-validate="description"></textarea>
                    </div>
                </div>


                <div class="ui divider"></div>

                <div class="buttonDiv">
                    <button class="ui primary submit button fluid" value="upload" type="submit">Submit</button>
                    <button class="ui primary loading button fluid" style="display: none;">Loading</button>
                </div>

                <div class="ui error message"></div>
            </div>
        </form>

        <!--<form action="control/fileUpload.php" name="imageUpload" class="ui form" method="POST" enctype="multipart/form-data">
            <div class="required field">
                <label>Image Of the Book</label>
                <input class="button" name="bookImage" type="file" id="fileinput" />
                <button class="ui basic button primary" value="upload" type="submit">Upload</button>
            </div>
        </form>-->
    </div>
</div>
</body>
<script src="assets/js/formValidation.js"></script>
</html>